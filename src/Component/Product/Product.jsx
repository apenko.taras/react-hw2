import React, {Component} from 'react';
import Btn from "../Btn/Btn";
import  "./Product.scss"
import Favorite from "../Favorite/Favorite";
import PropTypes from "prop-types";

class Product extends Component {
    handleClickBtn = () => {
        // this.props.addToCart(this.props.name)
        this.props.showModal('first', this.props.name)
    }

    handleFavorite = () => {
        console.log('handleClickFavorite)))))))))))');
        this.props.addToFavorite(this.props.index)
    }

    render() {
        const { name,
                price,
                urlPic,
                index,
                favorite,
                // addToFavorite,
                // showModal,
        } = this.props;

        return (
            <div className="product-item">
                <div className="product-img"><img className="product-img__item" src={urlPic}/></div>
                <div className="product-list">
                    <h3 className="product-name">{name}</h3>
                    <span className="product-price">Price: {price}</span>
                    <div className="product-buttons">
                        <div className="product-cart">
                            {/*<Btn text="Add to cart" backgroundColor="red" onClick={this.handleClickBtn}/>*/}
                            <Btn text="Add to cart" backgroundColor="red" onClick={this.handleClickBtn}/>
                        </div>
                        <div className="product-btn__favorite" onClick={this.handleFavorite}>
                            {/*{star("grey", "prod__svg")}*/}
                            <Favorite index={index} favorite={favorite} />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

Product.propTypes = {
    name: PropTypes.string,
    price: PropTypes.number,
    urlPic: PropTypes.string,
    index: PropTypes.number,
    favorite: PropTypes.object,
}

export default Product;
