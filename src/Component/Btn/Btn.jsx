import React, {Component} from 'react';
import './Btn.scss'

class Btn extends Component {

    render() {
        const {backgroundColor, text, onClick} = this.props;

        return (
            <button
                className='btnMod'
                onClick={onClick}
                style = {{backgroundColor:backgroundColor}}
            >{text}</button>
        )
    }
}

export default Btn