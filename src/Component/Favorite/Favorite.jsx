import React from "react";
import {star} from "../../Icons/star";
import PropTypes from "prop-types";
import Product from "../Product/Product";

function Favorite ({index, favorite}) {

    if (favorite[index] === true) {
        return (
            star("gold", "prod__svg")
        )
    } else return star("grey", "prod__svg")
}

Favorite.propTypes = {
    index: PropTypes.number,
    favorite: PropTypes.object,
}

export default Favorite